//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ELImageProcessor
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProductAttributeValueMap
    {
        public long AutoID { get; set; }
        public long Product_AutoID { get; set; }
        public long ProductAttribute_AutoID { get; set; }
        public Nullable<long> ProductAttributeVAlue_AutoID { get; set; }
    
        public virtual AttributeValue AttributeValue { get; set; }
        public virtual Product Product { get; set; }
    }
}
